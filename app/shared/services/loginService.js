// Definition des Service loginService
app.service('loginService', ['$location', '$firebaseAuth', '$firebaseObject', '$firebaseArray', function($location, $firebaseAuth, $firebaseObject, $firebaseArray){

    var context = this;
    var user = firebase.User;
    user = $firebaseAuth().$getAuth();

    // set authenticated user
    // Login User mit Firebase Authentication
    context.userLogin = function(email, password){
        $firebaseAuth().$signInWithEmailAndPassword(email, password)
            .then(function(user){
                console.log("The User " + user.uid + " got logged in successfully");
                // Route programmatisch wechseln zu chat
                $location.path('/chat');
            })
            .catch(function (error) {
                console.log(error);
            })
    };

    // neuen User registrieren mit Firebase Authentication
    context.userRegister = function(firstname, lastname, email, birthday, password){
        $firebaseAuth().$createUserWithEmailAndPassword(email, password)
            .then(function(user) {
                console.log( "User created with uid: " + user.uid);
                context.saveUserInfoFromForm(user.uid, firstname, lastname, birthday);
                context.userLogin(email, password);
            }).catch(function(error) {
            console.log = error;
        });
    };

    // zusätzliche Nutzerdaten in Firebase Datenbank speichern für neu registrierten User
    context.saveUserInfoFromForm = function(uid, firstname, lastname, birthday){
        const userDataRef = firebase.database().ref().child('userData').child(uid);

        userDataRef.set({
            firstname: firstname,
            lastname: lastname,
            birthday: birthday.getTime()
        })
            .then(function(){
                console.log("User Data got saved ");
            })
            .catch(function (error) {
                console.log(error);
            });

    };

    // Logout User mit firebase Authentication
    context.userLogout = function(){
        $firebaseAuth().$signOut()
            .then(function(){
                console.log("User got logged out successfully");
                $location.path('/logout');
            })
            .catch(function(error){
                console.log(error);
            })
    };

    // Return eingeloggten User mit Firebase Authentication
    context.getCurrentUser = function(){
        return $firebaseAuth().$getAuth();
    };

    // Return Nutzerdaten der Firebase Datenbank für ausgewählten User
    context.getUserData = function(uid){
        const userDataRef = firebase.database().ref().child('userData').child(uid);
        return $firebaseObject(userDataRef);
    };
}]);
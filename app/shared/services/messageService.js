// Service messageService
app.service('messageService', ['$firebaseArray', function($firebaseArray){
    var context = this;
    var database = firebase.database();
    const messagesRef = database.ref().child('messages').limitToLast(50);
    context.messages = $firebaseArray(messagesRef);

    // neue Nachricht speichern mit Firebase Datenbank
    context.saveMessage = function(content, uid){
        var message = {
            content: content,
            uid: uid,
            timestamp: Date.now()
        };
        context.messages.$add(message);
    };

    // Return messages
    context.getMessages = function(){
        return context.messages;
    };
}]);
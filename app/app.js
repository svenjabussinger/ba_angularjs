// Hauptmodul app
'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.version',
    'firebase'
]);

// Konfiguration der Firebase Datenbank
app.config(function(){
    var config = {
        apiKey: "AIzaSyBuFdA9Vaezoqm5G5xQ4Ztq8kj05ZOmNzY",
        authDomain: "chat-app-92b79.firebaseapp.com",
        databaseURL: "https://chat-app-92b79.firebaseio.com",
        projectId: "chat-app-92b79",
        storageBucket: "chat-app-92b79.appspot.com",
        messagingSenderId: "297794186257"
    };
    firebase.initializeApp(config);
});
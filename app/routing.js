// Konfiguration des Routing über $routeProvider

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when("/", {
            templateUrl:"views/chatView.html"
        })
        .when("/login", {
            templateUrl: "views/loginView.html"
        })
        .when("/chat", {
            templateUrl: "views/chatView.html"
        })
        .when("/logout", {
            templateUrl: "views/logoutView.html"
        })
}]);
// Controller der LoginView
app.controller('loginViewCtrl', ['$scope', function($scope){
    $scope.loginView = true;

    $scope.changeLogin = function(){
        if($scope.loginView === true){
            $scope.loginView = false;
        }else{
            $scope.loginView = true;
        }
    }
}]);

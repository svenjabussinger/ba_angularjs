//Controller der message Komponente
function MessageController(loginService) {
    var ctrl = this;

    ctrl.$onInit = function(){
        ctrl.userID = loginService.getCurrentUser().uid;
        ctrl.checkUsersMessage();

        ctrl.messageUser = loginService.getUserData(ctrl.msg.uid);
    };

    ctrl.checkUsersMessage = function () {
        if(ctrl.userID === ctrl.msg.uid){
            ctrl.isUsersMessage = true;
        }else {
            ctrl.isUsersMessage = false;
        }
    }

}
// Dependency Injection des loginService
MessageController.$inject = ['loginService'];

// message Komponente mit input binding von msg
app.component('message', {
   templateUrl: 'components/message/message.html',
    controller: MessageController,
    bindings: {
       msg: '<'
    }
});
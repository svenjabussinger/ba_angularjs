// Controller der Register Komponente
function RegisterController(loginService){
    var ctrl = this;

    ctrl.user = {
        firstname: '',
        lastname: '',
        email: '',
        birthday: '',
        password: ''
    }

    ctrl.registerUser = function(){
        loginService.userRegister(ctrl.user.firstname, ctrl.user.lastname, ctrl.user.email, ctrl.user.birthday, ctrl.user.password);
    }
}
// Dependency Injection des loginService
RegisterController.$inject = ['loginService'];

// Register Komponente
app.component('register', {
   templateUrl: "components/register/register.html",
    controller: RegisterController
});
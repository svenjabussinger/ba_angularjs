// Controller für Chatheader Komponente
function ChatheaderController(loginService){
    var ctrl = this;

    ctrl.logout = function(){
        loginService.userLogout();
    }
}
// Dependency Injection des loginService
ChatheaderController.$inject = ['loginService'];

// Chatheader Komponente
app.component('chatheader', {
   templateUrl: 'components/chatheader/chatheader.html',
    controller: ChatheaderController
});

//Controller für chat Komponente
function ChatController(messageService, loginService, $location){
    var ctrl = this;

    // Implementierung der Lebenszyklusmethode $onInit
    ctrl.$onInit = function(){
        ctrl.currentUser = loginService.getCurrentUser();

        if(ctrl.currentUser === null){
            $location.path('/login');
        }else {
            ctrl.currentUserID = ctrl.currentUser.uid;
            ctrl.currentUserData = loginService.getUserData(ctrl.currentUserID);

            ctrl.messages = messageService.getMessages();

            ctrl.scrollToBottom();
        }
    };


    ctrl.scrollToBottom = function () {

    }
}

// Dependency Injection des messageService, loginService und $location
ChatController.$inject = ['messageService', 'loginService', '$location'];

// chat Komponente
app.component('chat', {
   templateUrl: 'components/chat/chat.html',
    controller: ChatController
});

// zusätzliche Scroll Direktive für message panel
app.directive('scrollToBottom', function($timeout, $window) {
    return {
        scope: {
            scrollToBottom: "="
        },
        restrict: 'A',
        link: function(scope, element, attr) {
            scope.$watchCollection('scrollToBottom', function(newVal) {
                if (newVal) {
                    $timeout(function() {
                        element[0].scrollTop =  element[0].scrollHeight;
                    }, 0);
                }

            });

        }
    };
});
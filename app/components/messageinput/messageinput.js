// Controller der Messageinput Komponente
function MessageinputController(messageService, loginService){
    var ctrl = this;

    ctrl.newmessage = '';

    ctrl.sendNewMessage = function(){
        messageService.saveMessage(ctrl.newmessage, loginService.getCurrentUser().uid);
        ctrl.newmessage = '';
    }

    ctrl.sendNewMessage = function($event){
        if ($event.keyCode === 13){
            messageService.saveMessage(ctrl.newmessage, loginService.getCurrentUser().uid);
            ctrl.newmessage = '';
        }
    }
}
// Dependency Injection des messageService und loginService
MessageinputController.$inject = ['messageService', 'loginService'];

// messageinput Komponente
app.component('messageinput', {
   templateUrl: 'components/messageinput/messageinput.html',
    controller: MessageinputController
});
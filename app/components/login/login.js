//Controller der login Komponente
function LoginController(loginService){
    var ctrl = this;

    ctrl.login = {
        email: '',
        password: ''
    }

    ctrl.loginUser = function(){
        loginService.userLogin(ctrl.login.email, ctrl.login.password);
    }
}
// Dependency Injection des loginService
LoginController.$inject = ['loginService'];

// Login Komponente
app.component('login', {
   templateUrl: 'components/login/login.html',
    controller: LoginController
});